#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
apt update && apt upgrade -y

# setup nginx
apt install nginx -y
cp nginx/nginx.conf /etc/nginx/nginx.conf
cp nginx/sites-enabled/* /etc/nginx/sites-enabled
service nginx reload

# setup flask
apt install python3 -y
apt install python3-pip -y
pip3 install flask
pip3 install gunicorn

# create flask project
mkdir /var/app
cp flask/* /var/app
mkdir /var/app/static
cp flask/static/* /var/app/static -r
mkdir /var/app/templates
cp flask/templates/* /var/app/templates

# create flask service
cp whitelist-api.service /etc/systemd/system
systemctl daemon-reload
service whitelist-api start

# setup iptables
apt install iptables-persistent -y
pip3 install python-iptables 
cp rules.v4 /etc/iptables/rules.v4

# setup iptables auto backup
echo "/sbin/iptables-restore < /etc/iptables/rules.v4" > rc.local
echo "* * * * * /sbin/iptables-save > /etc/iptables/rules.v4" >> /var/spool/cron/crontabs/root
/sbin/iptables-restore < /etc/iptables/rules.v4
unset DEBIAN_FRONTEND