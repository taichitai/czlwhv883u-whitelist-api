from flask import Flask, request, render_template, send_file
from markupsafe import escape
from iptables import Iptables
from config import Config

# Variables
whitelist_chain_name = 'WHITELIST'
api_chain_name = 'API'


# Web Application
app = Flask(__name__)


@app.route('/')
def api_status():
    return render_template('swaggerui.html')


@app.route("/spec")
def spec():
    return send_file('static/yaml/swagger.yaml')


@app.route('/whitelist', methods=['GET'])
def list_whitelisted_ip():
    iptables = Iptables(whitelist_chain_name)

    ip_only = escape(request.args.get('ip_only'))
    if ip_only == 'true':
        ip_only = True
    else:
        ip_only = False

    debug = escape(request.args.get('debug'))
    if debug == 'true':
        debug = True
    else:
        debug = False

    return {'whitelist': iptables.get_whitelist(ip_only=ip_only, debug=debug)}


@app.route('/whitelist', methods=['POST'])
def add_whitelisted_ip():
    ip = escape(request.form.get('ip'))

    iptables = Iptables(whitelist_chain_name)
    result = iptables.add_whitelist(ip)

    if result is True:
        config = Config()
        response = {
            'success': result,
            'ip': config.get_random_ip()
        }

    else:
        response = {
            'success': False,
            'error_message': result
        }

    return response


@app.route('/revoke', methods=['POST'])
def revoke_whitelisted_ip():
    ip = escape(request.form.get('ip'))

    iptables = Iptables(whitelist_chain_name)
    result = iptables.remove_whitelist(ip)

    if result is True:
        response = {
            'success': result,
            'ip': ip
        }

    else:
        response = {
            'success': False,
            'error_message': result
        }

    return response


@app.route('/api_access', methods=['GET'])
def list_api_whitelisted_ip():
    iptables = Iptables(api_chain_name)

    ip_only = escape(request.args.get('ip_only'))
    if ip_only == 'true':
        ip_only = True
    else:
        ip_only = False

    debug = escape(request.args.get('debug'))
    if debug == 'true':
        debug = True
    else:
        debug = False

    return {'whitelist': iptables.get_whitelist(ip_only=ip_only, debug=debug)}


@app.route('/api_access', methods=['POST'])
def add_api_whitelisted_ip():
    ip = escape(request.form.get('ip'))

    iptables = Iptables(api_chain_name)
    result = iptables.add_whitelist(ip)

    if result is True:
        response = {
            'success': result,
            'ip': ip
        }

    else:
        response = {
            'success': False,
            'error_message': result
        }

    return response


@app.route('/revoke_api_access', methods=['POST'])
def revoke_api_whitelisted_ip():
    ip = escape(request.form.get('ip'))

    iptables = Iptables(api_chain_name)
    result = iptables.remove_whitelist(ip)

    if result is True:
        response = {
            'success': result,
            'ip': ip
        }

    else:
        response = {
            'success': False,
            'error_message': result
        }

    return response


@app.route('/blocked', methods=['GET'])
def get_logs():
    lines = escape(request.args.get('lines'))

    if lines == 'None':
        lines = 20
    else:
        lines = int(lines)

    iptables = Iptables()
    result = iptables.get_logs()

    result = result[-lines:]

    return {'log': result}


if __name__ == '__main__':
    app.run()
