import iptc
import ipaddress


class Iptables:
	def __init__(self, chain_name='INPUT', log_file_path='/var/log/kern.log', log_pharse='IPTables-Dropped: '):
		self.chain_name = chain_name
		self.log_file_path = log_file_path
		self.log_pharse = log_pharse

	def get_whitelist(self, ip_only=False, debug=False):
		raw_whitelist_records = iptc.easy.dump_chain('filter', self.chain_name)

		if ip_only is True:
			whitelist = []
			for record in raw_whitelist_records:
				whitelist.append(record['src'])

			return whitelist

		if debug is not True:
			whitelist_records = []
			for record in raw_whitelist_records:
				del record['counters']
				whitelist_records.append(record)

		else:
			whitelist_records = raw_whitelist_records

		return whitelist_records

	def add_whitelist(self, ip):
		try:
			ip = ipaddress.ip_network(ip)
			ip = str(ip)
		except Exception as err:
			return str(err)

		whitelist = self.get_whitelist(ip_only=True)
		if ip in whitelist:
			return 'IP already whitelisted'

		rule_d = {
			'target': 'ACCEPT',
			'src': ip
		}

		iptc.easy.insert_rule('filter', self.chain_name, rule_d)
		return True

	def remove_whitelist(self, ip):
		try:
			ip = ipaddress.ip_network(ip)
			ip = str(ip)
		except Exception as err:
			return str(err)

		whitelist = self.get_whitelist(ip_only=True)

		if ip not in whitelist:
			return 'IP is not whitelisted'

		rule_d = {
			'target': 'ACCEPT',
			'src': ip
		}

		iptc.easy.delete_rule('filter', self.chain_name, rule_d)
		return True

	def get_logs(self):
		with open(self.log_file_path) as file:
			file = file.readlines()

		log_file = []
		for line in file:
			if self.log_pharse in line:
				log_file.append(line)
		
		return log_file
