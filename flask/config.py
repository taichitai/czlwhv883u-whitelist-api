import re
import random


class Config:
    def __init__(self, config_path='/etc/nginx/nginx.conf'):
        self.config_path = config_path

    def get_all_ip(self):
        config_file = open(self.config_path, 'r')
        config = config_file.read()
        config_file.close()

        config = re.search(r'backend_30199 {.+}\n\n\tupstream', config, re.DOTALL).group(0)
        server_list = []
        for line in config.split('\n'):
            if 'server' in line:
                ip = line.strip()
                ip = ip.split(' ')[1]
                ip = ip.split(':')[0]
                server_list.append(ip)

        return server_list

    def get_random_ip(self):
        ip = self.get_all_ip()
        return random.choice(ip)
